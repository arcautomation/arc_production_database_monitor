package ServerMonitoring;


import java.awt.AWTException;
import java.awt.Color;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class RabbitMQ {
	
	static WebDriver driver;
	
	public static void browserLaunch()
	{
		try
		{
		File dest = new File("C:\\Users\\trinanjwan.sarkar\\eclipse-workspace\\Trinanjwan's_ServerMonitoringProject\\Driver\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver( options );
		}
		catch(Exception e)
		{
			e.getCause();
		}
		
	}
	
	public static void lauchApp() throws IOException, InterruptedException
	{
		try
		{
		driver.get(PropertyReader.getProperty("RabbitMQURL"));
		//Thread.sleep(5000);
		}
		catch(Exception e)
		{
			e.getCause();
		}
	}
	
	public static void login() throws InterruptedException, IOException
	{
		try
		{
		WebElement userName = driver.findElement(By.name("username"));
		WebElement password = driver.findElement(By.name("password"));
		WebElement loginButton = driver.findElement(By.cssSelector("#login>form>p>input"));
		userName.clear();
		userName.sendKeys(PropertyReader.getProperty("Username"));
		password.clear();
		password.sendKeys(PropertyReader.getProperty("Password"));
		loginButton.click();
		//Thread.sleep(5000);
	}
	catch(Exception e)
	{
		e.getCause();
	}
		
	}
	
	public static void searchService1ReadyCount(String servicename1) throws IOException, AWTException, InterruptedException
	{
		
//		WebElement queueSearchbox = driver.findElement(By.cssSelector("#queues-name"));
//		queueSearchbox.clear();
//		//Thread.sleep(5000);
//		queueSearchbox.sendKeys(PropertyReader.getProperty("Service1"));
//		Thread.sleep(3000);
//		Robot rb = new Robot();
//		rb.keyPress(KeyEvent.VK_ENTER);
//		rb.keyRelease(KeyEvent.VK_ENTER);
//		Thread.sleep(14000);
		
		
		Thread.sleep(5000);
		driver.navigate().refresh();
		Thread.sleep(5000);
		WebElement readyCount = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/table/tbody/tr[1]/td[5]"));
		
		WebDriverWait wait=new WebDriverWait(driver,50);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(readyCount)));
		
		System.out.println("Count for "+ PropertyReader.getProperty("Service1")+": "+ readyCount.getText());
		//int readyCount1 = Integer.parseInt(readyCount.getText());
		int readyCount1 = Integer.parseInt(readyCount.getText().replaceAll(",", ""));
		
		String readycountst1= Integer.toString(readyCount1);
		System.setProperty("rabbit1", readycountst1);
		System.out.println("Count for "+ PropertyReader.getProperty("Service1")+": "+ readyCount1);
		
		if(readyCount1>200)
		{
			String line1 = "Count goes more than 200 for the QUEUE: "+" "+ PropertyReader.getProperty("Service1")+"."+" Current count is"+" "+ readyCount.getText();
			//Utilities.addColor(line1, Color.BLUE);
			//Utilities.fireMail(addColor(line1, Color.BLUE));
			
			Mailtrigger.rabbitmail(line1, servicename1);
		//	Mailtrigger.main(line1, servicename);

	}


	}
	
	public static void searchService2ReadyCount(String servicename) throws IOException, AWTException, InterruptedException
	{
	
//		WebElement queueSearchbox = driver.findElement(By.cssSelector("#queues-name"));
//		queueSearchbox.clear();
//		//Thread.sleep(5000);
//		queueSearchbox.sendKeys(PropertyReader.getProperty("Service2"));
//		//Thread.sleep(5000);
//		Robot rb = new Robot();
//		rb.keyPress(KeyEvent.VK_ENTER);
//		rb.keyRelease(KeyEvent.VK_ENTER);
//		Thread.sleep(14000);
		
		//Thread.sleep(5000);
		driver.navigate().refresh();
		Thread.sleep(5000);
		WebElement readyCount = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/table/tbody/tr[2]/td[5]"));
		WebDriverWait wait=new WebDriverWait(driver,50);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(readyCount)));
		
		System.out.println("Count for "+ PropertyReader.getProperty("Service2")+": "+ readyCount.getText());
		
		//int readyCount1 = Integer.parseInt(readyCount.getText());
		int readyCount1 = Integer.parseInt(readyCount.getText().replaceAll(",", ""));
		String readycountst1= Integer.toString(readyCount1);
		System.setProperty("rabbit1", readycountst1);
		
		
		if(readyCount1>200)
		{
			String line1 = "Count goes more than 200 for the QUEUE: "+" "+ PropertyReader.getProperty("Service2")+"."+" Current count is"+" "+ readyCount.getText();
			//Utilities.addColor(line1, Color.BLUE);
			//Utilities.fireMail(addColor(line1, Color.BLUE));
			Mailtrigger.rabbitmail(line1, servicename);
		}
		
		


	}
	
	public static void searchService3ReadyCount(String servicename) throws IOException, AWTException, InterruptedException
	{
		
//		WebElement queueSearchbox = driver.findElement(By.cssSelector("#queues-name"));
//		queueSearchbox.clear();
//		//Thread.sleep(5000);
//		queueSearchbox.sendKeys(PropertyReader.getProperty("Service3"));
//		Thread.sleep(3000);
//		Robot rb = new Robot();
//		rb.keyPress(KeyEvent.VK_ENTER);
//		rb.keyRelease(KeyEvent.VK_ENTER);
//		Thread.sleep(14000);
		
		//Thread.sleep(5000);
		driver.navigate().refresh();
		Thread.sleep(5000);
		WebElement readyCount = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/table/tbody/tr[3]/td[5]"));
		WebDriverWait wait=new WebDriverWait(driver,50);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(readyCount)));
		
		System.out.println("Count for "+ PropertyReader.getProperty("Service3")+": "+ readyCount.getText());
		
		int readyCount1 = Integer.parseInt(readyCount.getText().replaceAll(",", ""));
		String readycountst1= Integer.toString(readyCount1);
		System.setProperty("rabbit1", readycountst1);
		
		if(readyCount1>200)
		{
			String line1 = "Count goes more than 200 for the QUEUE: "+" "+ PropertyReader.getProperty("Service3")+"."+" Current count is"+" "+ readyCount.getText();
			//Utilities.addColor(line1, Color.BLUE);
			//Utilities.fireMail(addColor(line1, Color.BLUE));
			Mailtrigger.rabbitmail(line1, servicename);
		}

		
	}

	
	
	public static void searchService4ReadyCount(String servicename) throws IOException, AWTException, InterruptedException
	{
	
//		WebElement queueSearchbox = driver.findElement(By.cssSelector("#queues-name"));
//		queueSearchbox.clear();
//		//Thread.sleep(5000);
//		queueSearchbox.sendKeys(PropertyReader.getProperty("Service4"));
//		Thread.sleep(3000);
//		Robot rb = new Robot();
//		rb.keyPress(KeyEvent.VK_ENTER);
//		rb.keyRelease(KeyEvent.VK_ENTER);
//		Thread.sleep(14000);
		
		
		//Thread.sleep(5000);
		driver.navigate().refresh();
		Thread.sleep(5000);
		WebElement readyCount = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/table/tbody/tr[4]/td[5]"));
		WebDriverWait wait=new WebDriverWait(driver,50);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(readyCount)));
		
		System.out.println("Count for "+ PropertyReader.getProperty("Service4")+": "+ readyCount.getText());
		
		int readyCount1 = Integer.parseInt(readyCount.getText().replaceAll(",", ""));
		String readycountst1= Integer.toString(readyCount1);
		System.setProperty("rabbit1", readycountst1);
		
		if(readyCount1>200)
		{
			String line1 = "1. Count goes more than 200 for the QUEUE: "+" "+ PropertyReader.getProperty("Service4")+"."+" Current Count is"+" "+ readyCount.getText();
			//Utilities.addColor(line1, Color.BLUE);
			//Utilities.fireMail(addColor(line1, Color.BLUE));
			Mailtrigger.rabbitmail(line1, servicename);
		}

	
	}
	
	public static void searchService5ReadyCount(String servicename) throws IOException, AWTException, InterruptedException
	{
		
//		WebElement queueSearchbox = driver.findElement(By.cssSelector("#queues-name"));
//		queueSearchbox.clear();
//		//Thread.sleep(5000);
//		queueSearchbox.sendKeys(PropertyReader.getProperty("Service5"));
//		Thread.sleep(3000);
//		Robot rb = new Robot();
//		rb.keyPress(KeyEvent.VK_ENTER);
//		rb.keyRelease(KeyEvent.VK_ENTER);
//		Thread.sleep(14000);
		
		//Thread.sleep(5000);
		driver.navigate().refresh();
		Thread.sleep(5000);
		WebElement readyCount = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/table/tbody/tr[5]/td[5]"));
		WebDriverWait wait=new WebDriverWait(driver,50);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(readyCount)));
		
		
		System.out.println("Count for "+ PropertyReader.getProperty("Service5")+": "+ readyCount.getText());
		
		int readyCount1 = Integer.parseInt(readyCount.getText().replaceAll(",", ""));
		String readycountst1= Integer.toString(readyCount1);
		System.setProperty("rabbit1", readycountst1);
		
		if(readyCount1>200)
		{
			String line1 = "1. Count goes more than 200 for the QUEUE: "+" "+ PropertyReader.getProperty("Service5")+"."+" Current Count is"+" "+ readyCount.getText();
			//Utilities.addColor(line1, Color.BLUE);
			//Utilities.fireMail(addColor(line1, Color.BLUE));
			Mailtrigger.rabbitmail(line1, servicename);
		}


	}
	
	public static void searchService6ReadyCount(String servicename) throws IOException, AWTException, InterruptedException
	{
	
//		WebElement queueSearchbox = driver.findElement(By.cssSelector("#queues-name"));
//		queueSearchbox.clear();
//		//Thread.sleep(5000);
//		queueSearchbox.sendKeys(PropertyReader.getProperty("Service6"));
//		Thread.sleep(3000);
//		Robot rb = new Robot();
//		rb.keyPress(KeyEvent.VK_ENTER);
//		rb.keyRelease(KeyEvent.VK_ENTER);
//		Thread.sleep(14000);
		
		////Thread.sleep(5000);
		driver.navigate().refresh();
		Thread.sleep(5000);
		WebElement readyCount = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/table/tbody/tr[6]/td[5]"));
		WebDriverWait wait=new WebDriverWait(driver,50);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(readyCount)));
		
		System.out.println("Count for "+ PropertyReader.getProperty("Service6")+": "+ readyCount.getText());
		
		int readyCount1 = Integer.parseInt(readyCount.getText().replaceAll(",", ""));
		String readycountst1= Integer.toString(readyCount1);
		System.setProperty("rabbit1", readycountst1);
		
		if(readyCount1>200)
		{
			String line1 = "1. Count goes more than 200 for the QUEUE: "+" "+ PropertyReader.getProperty("Service6")+"."+" Current Count is"+" "+ readyCount.getText();
			//Utilities.addColor(line1, Color.BLUE);
			//Utilities.fireMail(addColor(line1, Color.BLUE));
			Mailtrigger.rabbitmail(line1, servicename);
		}

	}
	
	public static void searchService7ReadyCount(String servicename) throws IOException, AWTException, InterruptedException
	{
	
//		WebElement queueSearchbox = driver.findElement(By.cssSelector("#queues-name"));
//		queueSearchbox.clear();
//		//Thread.sleep(5000);
//		queueSearchbox.sendKeys(PropertyReader.getProperty("Service7"));
//		Thread.sleep(3000);
//		Robot rb = new Robot();
//		rb.keyPress(KeyEvent.VK_ENTER);
//		rb.keyRelease(KeyEvent.VK_ENTER);
//		Thread.sleep(14000);
		
		//Thread.sleep(5000);
		driver.navigate().refresh();
		Thread.sleep(5000);
		WebElement readyCount = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/table/tbody/tr[7]/td[5]"));
		WebDriverWait wait=new WebDriverWait(driver,50);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(readyCount)));
		System.out.println("Count for "+ PropertyReader.getProperty("Service7")+": "+ readyCount.getText());
		
		int readyCount1 = Integer.parseInt(readyCount.getText().replaceAll(",", ""));
		String readycountst1= Integer.toString(readyCount1);
		System.setProperty("rabbit1", readycountst1);
		
		if(readyCount1>200)
		{
			String line1 = "1. Count goes more than 200 for the QUEUE: "+" "+ PropertyReader.getProperty("Service7")+"."+" Current Count is"+" "+ readyCount.getText();
			//Utilities.addColor(line1, Color.BLUE);
			//Utilities.fireMail(addColor(line1, Color.BLUE));
			Mailtrigger.rabbitmail(line1, servicename);
		}

	}
	
	public static void searchService8ReadyCount(String servicename) throws IOException, AWTException, InterruptedException
	{
		
//		WebElement queueSearchbox = driver.findElement(By.cssSelector("#queues-name"));
//		queueSearchbox.clear();
//		//Thread.sleep(5000);
//		queueSearchbox.sendKeys(PropertyReader.getProperty("Service8"));
//		Thread.sleep(3000);
//		Robot rb = new Robot();
//		rb.keyPress(KeyEvent.VK_ENTER);
//		rb.keyRelease(KeyEvent.VK_ENTER);
//		Thread.sleep(14000);
		
		//Thread.sleep(5000);
		driver.navigate().refresh();
		Thread.sleep(5000);
		WebElement readyCount = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/table/tbody/tr[8]/td[5]"));
		WebDriverWait wait=new WebDriverWait(driver,50);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(readyCount)));
		
		System.out.println("Count for "+ PropertyReader.getProperty("Service8")+": "+ readyCount.getText());
		
		int readyCount1 = Integer.parseInt(readyCount.getText().replaceAll(",", ""));
		String readycountst1= Integer.toString(readyCount1);
		System.setProperty("rabbit1", readycountst1);
		
		if(readyCount1>200)
		{
			String line1 = "1. Count goes more than 200 for the QUEUE: "+" "+ PropertyReader.getProperty("Service8")+"."+" Current Count is"+" "+ readyCount.getText();
			//Utilities.addColor(line1, Color.BLUE);
			//Utilities.fireMail(addColor(line1, Color.BLUE));
			Mailtrigger.rabbitmail(line1, servicename);
		}

	}
	
	public static void searchService9ReadyCount(String servicename) throws IOException, AWTException, InterruptedException
	{
		
//		WebElement queueSearchbox = driver.findElement(By.cssSelector("#queues-name"));
//		queueSearchbox.clear();
//		//Thread.sleep(5000);
//		queueSearchbox.sendKeys(PropertyReader.getProperty("Service9"));
//		Thread.sleep(3000);
//		Robot rb = new Robot();
//		rb.keyPress(KeyEvent.VK_ENTER);
//		rb.keyRelease(KeyEvent.VK_ENTER);
//		Thread.sleep(14000);
		
		//Thread.sleep(5000);
		driver.navigate().refresh();
		Thread.sleep(5000);
		WebElement readyCount = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/table/tbody/tr[9]/td[5]"));
		WebDriverWait wait=new WebDriverWait(driver,50);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(readyCount)));
		
		System.out.println("Count for "+ PropertyReader.getProperty("Service9")+": "+ readyCount.getText());
		
		int readyCount1 = Integer.parseInt(readyCount.getText().replaceAll(",", ""));
		String readycountst1= Integer.toString(readyCount1);
		System.setProperty("rabbit1", readycountst1);
		
		if(readyCount1>200)
		{
			String line1 = "1. Count goes more than 200 for the QUEUE: "+" "+ PropertyReader.getProperty("Service9")+"."+" Current Count is"+" "+ readyCount.getText();
			//Utilities.addColor(line1, Color.BLUE);
			//Utilities.fireMail(addColor(line1, Color.BLUE));
			Mailtrigger.rabbitmail(line1, servicename);
		}
		

	}
	
	public static void searchService10ReadyCount(String servicename) throws IOException, AWTException, InterruptedException
	{
		
//		WebElement queueSearchbox = driver.findElement(By.cssSelector("#queues-name"));
//		queueSearchbox.clear();
//		//Thread.sleep(5000);
//		queueSearchbox.sendKeys(PropertyReader.getProperty("Service10"));
//		Thread.sleep(3000);
//		Robot rb = new Robot();
//		rb.keyPress(KeyEvent.VK_ENTER);
//		rb.keyRelease(KeyEvent.VK_ENTER);
//		Thread.sleep(14000);
		
		//Thread.sleep(5000);
		driver.navigate().refresh();
		Thread.sleep(5000);
		WebElement readyCount = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/table/tbody/tr[10]/td[5]"));
		WebDriverWait wait=new WebDriverWait(driver,50);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(readyCount)));
		
		System.out.println("Count for "+ PropertyReader.getProperty("Service10")+": "+ readyCount.getText());
		
		int readyCount1 = Integer.parseInt(readyCount.getText().replaceAll(",", ""));
		String readycountst1= Integer.toString(readyCount1);
		System.setProperty("rabbit1", readycountst1);
		
		if(readyCount1>200)
		{
			String line1 = "1. Count goes more than 200 for the QUEUE: "+" "+ PropertyReader.getProperty("Service10")+"."+" Current Count is"+" "+ readyCount.getText();
			//Utilities.addColor(line1, Color.BLUE);
			//Utilities.fireMail(addColor(line1, Color.BLUE));
			Mailtrigger.rabbitmail(line1, servicename);
		}

	}
	
	public static void searchService11ReadyCount(String servicename) throws IOException, AWTException, InterruptedException
	{
		
//		WebElement queueSearchbox = driver.findElement(By.cssSelector("#queues-name"));
//		queueSearchbox.clear();
//		//Thread.sleep(5000);
//		queueSearchbox.sendKeys(PropertyReader.getProperty("Service11"));
//		Thread.sleep(3000);
//		Robot rb = new Robot();
//		rb.keyPress(KeyEvent.VK_ENTER);
//		rb.keyRelease(KeyEvent.VK_ENTER);
//		Thread.sleep(14000);
		
		//Thread.sleep(5000);
		driver.navigate().refresh();
		Thread.sleep(5000);
		WebElement readyCount = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/table/tbody/tr[11]/td[5]"));
		WebDriverWait wait=new WebDriverWait(driver,50);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(readyCount)));
		
		System.out.println("Count for "+ PropertyReader.getProperty("Service11")+": "+ readyCount.getText());
		
		int readyCount1 = Integer.parseInt(readyCount.getText().replaceAll(",", ""));
		String readycountst1= Integer.toString(readyCount1);
		System.setProperty("rabbit1", readycountst1);
		
		if(readyCount1>200)
		{
			String line1 = "1. Count goes more than 200 for the QUEUE: "+" "+ PropertyReader.getProperty("Service11")+"."+" Current Count is"+" "+ readyCount.getText();
			//Utilities.addColor(line1, Color.BLUE);
			//Utilities.fireMail(addColor(line1, Color.BLUE));
			Mailtrigger.rabbitmail(line1, servicename);
		}

	}
	
	public static void searchService12ReadyCount(String servicename) throws IOException, AWTException, InterruptedException
	{
		
//		WebElement queueSearchbox = driver.findElement(By.cssSelector("#queues-name"));
//		queueSearchbox.clear();
//		//Thread.sleep(5000);
//		queueSearchbox.sendKeys(PropertyReader.getProperty("Service12"));
//		Thread.sleep(3000);
//		Robot rb = new Robot();
//		rb.keyPress(KeyEvent.VK_ENTER);
//		rb.keyRelease(KeyEvent.VK_ENTER);
//		Thread.sleep(14000);
		
		//Thread.sleep(5000);
		driver.navigate().refresh();
		Thread.sleep(5000);
		WebElement readyCount = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/table/tbody/tr[13]/td[5]"));
		WebDriverWait wait=new WebDriverWait(driver,50);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(readyCount)));
		
		
		System.out.println("Count for "+ PropertyReader.getProperty("Service12")+": "+ readyCount.getText());
		
		int readyCount1 = Integer.parseInt(readyCount.getText().replaceAll(",", ""));
		String readycountst1= Integer.toString(readyCount1);
		System.setProperty("rabbit1", readycountst1);
		
		if(readyCount1>200)
		{
			String line1 = "1. Count goes more than 200 for the QUEUE: "+" "+ PropertyReader.getProperty("Service12")+"."+" Current Count is"+" "+ readyCount.getText();
			//Utilities.addColor(line1, Color.BLUE);
			//Utilities.fireMail(addColor(line1, Color.BLUE));
			Mailtrigger.rabbitmail(line1, servicename);
		}

	
	}
	
	public static void searchService13ReadyCount(String servicename) throws IOException, AWTException, InterruptedException
	{
		
//		WebElement queueSearchbox = driver.findElement(By.cssSelector("#queues-name"));
//		queueSearchbox.clear();
//		//Thread.sleep(5000);
//		queueSearchbox.sendKeys(PropertyReader.getProperty("Service13"));
//		Thread.sleep(3000);
//		Robot rb = new Robot();
//		rb.keyPress(KeyEvent.VK_ENTER);
//		rb.keyRelease(KeyEvent.VK_ENTER);
//		Thread.sleep(14000);
		
		//Thread.sleep(5000);
		driver.navigate().refresh();
		Thread.sleep(5000);
		WebElement readyCount = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/table/tbody/tr[16]/td[5]"));
		WebDriverWait wait=new WebDriverWait(driver,50);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(readyCount)));
		
		System.out.println("Count for "+ PropertyReader.getProperty("Service13")+": "+ readyCount.getText());
		
		int readyCount1 = Integer.parseInt(readyCount.getText().replaceAll(",", ""));
		String readycountst1= Integer.toString(readyCount1);
		System.setProperty("rabbit1", readycountst1);
		
		if(readyCount1>200)
		{
			String line1 = "1. Count goes more than 200 for the QUEUE: "+" "+ PropertyReader.getProperty("Service13")+"."+" Current Count is"+" "+ readyCount.getText();
			//Utilities.addColor(line1, Color.BLUE);
			//Utilities.fireMail(addColor(line1, Color.BLUE));
			Mailtrigger.rabbitmail(line1, servicename);
		}

	
	}
	
	public static void searchService14ReadyCount(String servicename) throws IOException, AWTException, InterruptedException
	{
		
		WebElement queueSearchbox = driver.findElement(By.cssSelector("#queues-name"));
		queueSearchbox.clear();
		//Thread.sleep(5000);
		queueSearchbox.sendKeys(PropertyReader.getProperty("Service14"));
		Thread.sleep(3000);
		Robot rb = new Robot();
		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(14000);
		
		WebElement readyCount = driver.findElement(By.xpath(".//*[@id='main']/div[2]/table/tbody/tr[1]/td[5]"));
		System.out.println("Count for "+ PropertyReader.getProperty("Service14")+": "+ readyCount.getText());
		
		int readyCount1 = Integer.parseInt(readyCount.getText().replaceAll(",", ""));
		String readycountst1= Integer.toString(readyCount1);
		System.setProperty("rabbit1", readycountst1);
		
		if(readyCount1>200)
		{
			String line1 = "Count goes more than 200 for the QUEUE: "+" "+ PropertyReader.getProperty("Service14")+"."+" Current count is"+" "+ readyCount.getText();
			//Utilities.addColor(line1, Color.BLUE);
			//Utilities.fireMail(addColor(line1, Color.BLUE));
			Mailtrigger.rabbitmail(line1, servicename);
		}


	}
	
	public static void searchService15ReadyCount(String servicename) throws IOException, AWTException, InterruptedException
	{
	
		WebElement queueSearchbox = driver.findElement(By.cssSelector("#queues-name"));
		queueSearchbox.clear();
		//Thread.sleep(5000);
		queueSearchbox.sendKeys(PropertyReader.getProperty("Service15"));
		Thread.sleep(3000);
		Robot rb = new Robot();
		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(14000);
		
		WebElement readyCount = driver.findElement(By.xpath(".//*[@id='main']/div[2]/table/tbody/tr[1]/td[5]"));
		System.out.println("Count for "+ PropertyReader.getProperty("Service15")+": "+ readyCount.getText());
		
		int readyCount1 = Integer.parseInt(readyCount.getText().replaceAll(",", ""));
		String readycountst1= Integer.toString(readyCount1);
		System.setProperty("rabbit1", readycountst1);
		
		if(readyCount1>200)
		{
			String line1 = "1. Count goes more than 200 for the QUEUE: "+" "+ PropertyReader.getProperty("Service15")+"."+" Current Count is"+" "+ readyCount.getText();
			//Utilities.addColor(line1, Color.BLUE);
			//Utilities.fireMail(addColor(line1, Color.BLUE));
			Mailtrigger.rabbitmail(line1, servicename);
		}

		
	}
	
	public static void searchService16ReadyCount(String servicename) throws IOException, AWTException, InterruptedException
	{
		
		WebElement queueSearchbox = driver.findElement(By.cssSelector("#queues-name"));
		queueSearchbox.clear();
		//Thread.sleep(5000);
		queueSearchbox.sendKeys(PropertyReader.getProperty("Service16"));
		Thread.sleep(3000);
		Robot rb = new Robot();
		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(14000);
		
		WebElement readyCount = driver.findElement(By.xpath(".//*[@id='main']/div[2]/table/tbody/tr[1]/td[5]"));
		System.out.println("Count for "+ PropertyReader.getProperty("Service16")+": "+ readyCount.getText());
		
		int readyCount1 = Integer.parseInt(readyCount.getText().replaceAll(",", ""));
		String readycountst1= Integer.toString(readyCount1);
		System.setProperty("rabbit1", readycountst1);
		
		if(readyCount1>200)
		{
			String line1 = "1. Count goes more than 200 for the QUEUE: "+" "+ PropertyReader.getProperty("Service16")+"."+" Current Count is"+" "+ readyCount.getText();
			//Utilities.addColor(line1, Color.BLUE);
			//Utilities.fireMail(addColor(line1, Color.BLUE));
			Mailtrigger.rabbitmail(line1, servicename);
		}

		
	}
	
	public static void dbConnect()
	{
		
	}
	
	public static void closeApp()
	{
		driver.quit();
	}


}
