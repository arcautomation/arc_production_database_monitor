package ServerMonitoring;

import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.Timer;

import org.openqa.selenium.WebDriver;

public class Driver_ServerMonitoring {
	static WebDriver driver;

	// dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT+5"));

	public static void main(String[] args) throws Exception {

		Timer time = new Timer(); // Instantiate Timer Object
		ScheduledTask st = new ScheduledTask(); // Instantiate SheduledTask class
		time.schedule(st, 0, 18000); // Create Repetitively task for every 1 secs0
		System.out.println("Before for");
		// for demo only.
		for (int i = 0; i <= 999999999; i++) {
			System.out.println("The value of i in the start: " + i);
			System.out.println("Execution in Main Thread...." + i);
			Date currentDate = new Date();
			SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy_MM_dd_HH_mm");
			String formattedDate = dateFormatGmt.format(currentDate);

			// Date date = new Date();
			// SimpleDateFormat sdf = new SimpleDateFormat("dd mm yyyy");
			dateFormatGmt.setTimeZone(TimeZone.getTimeZone("PST"));
			String dateInString = dateFormatGmt.format(currentDate);

			String fileName = "./Reports/Summary_" + dateInString + ".csv";
			FileWriter writer = new FileWriter(fileName);
			
			writer.append("Service");
			writer.append(",");
			writer.append("Count");
			writer.append("\n");
			
			writer.append("IMAGE TILES EXTRACTION SERVICES");
			writer.append("\n");
			
			// 002-Image tiles extraction_Inprogress Image tiles
			SQL_Test.InprogressImagetiles();
			writer.append("002-Image tiles extraction_Inprogress Image tiles");
			writer.append(",");
			String value2 = System.getProperty("total_count");
			writer.append(String.valueOf(value2));
			writer.append("\n");
			
			
			
			// 003-Image tiles extraction_Failed
			SQL_Test.FailedImagetiles();
			writer.append("003-Image tiles extraction_Failed");
			writer.append(",");
			String value3 = System.getProperty("total_count");
			writer.append(String.valueOf(value3));
			writer.append("\n");

			
			// 001-Image tiles extraction_Pending Image tiles
			SQL_Test.PendingImagetiles();
			writer.append("001-Image tiles extraction_Pending Image tiles");
			writer.append(",");
			String value1 = System.getProperty("total_count");
			writer.append(String.valueOf(value1));
			writer.append("\n");
	

			// 004-Image tiles extraction_Success
			SQL_Test.Successimagetilecount();
			writer.append("004-Image tiles extraction_Success");
			writer.append(",");
			String value4 = System.getProperty("total_count");
			writer.append(String.valueOf(value4));
			writer.append("\n");
			
			writer.append("CONVERSION SERVICES");
			writer.append("\n");

			

			// 007-Conversion service for the projects module_Documentinprocess
			SQL_Test.Documentinprocess();
			writer.append("007-Conversion service for the projects module_Documentinprocess");
			writer.append(",");
			String value7 = System.getProperty("total_count");
			writer.append(String.valueOf(value7));
			writer.append("\n");
			
			// 011-Conversion service for the projects module_Failed

			SQL_Test.DocumentFailed();
			writer.append("011-Conversion service for the projects module_Failed");
			writer.append(",");
			String value11 = System.getProperty("total_count");
			writer.append(String.valueOf(value11));
			writer.append("\n");

			// 008-Conversion service for the projects module_OCR Pending
			SQL_Test.DocumentOCRPending();
			writer.append("008-Conversion service for the projects module_OCR Pending");
			writer.append(",");
			String value8 = System.getProperty("total_count");
			writer.append(String.valueOf(value8));
			writer.append("\n");

			// 009-Conversion service for the projects module_Completed
			SQL_Test.DocumentCompleted();
			writer.append("009-Conversion service for the projects module_Completed");
			writer.append(",");
			String value9 = System.getProperty("total_count");
			writer.append(String.valueOf(value9));
			writer.append("\n");


		

			// 012-Conversion service for the projects module_FileSizeZero
			SQL_Test.DocumentFileSizeZero();
			writer.append("012-Conversion service for the projects module_FileSizeZero");
			writer.append(",");
			String value12 = System.getProperty("total_count");
			writer.append(String.valueOf(value12));
			writer.append("\n");

			// 013-Conversion service for the projects module_UploadFailed
			SQL_Test.DocumentUploadFailed();
			writer.append("013-Conversion service for the projects module_UploadFailed");
			writer.append(",");
			String value13 = System.getProperty("total_count");
			writer.append(String.valueOf(value13));
			writer.append("\n");

			// 014-Rotation files to be Processed
			SQL_Test.RotationToBeProcessed();
			writer.append("014-Rotation files to be Processed");
			writer.append(",");
			String value14 = System.getProperty("total_count");
			writer.append(String.valueOf(value14));
			writer.append("\n");

			// 015-Rotation files in Process
			SQL_Test.RotationInProcess();
			writer.append("015-Rotation files in Process");
			writer.append(",");
			String value15 = System.getProperty("total_count");
			writer.append(String.valueOf(value15));
			writer.append("\n");

			// 016-Rotation files Completed
			SQL_Test.RotationCompleted();
			writer.append("016-Rotation files Completed");
			writer.append(",");
			String value16 = System.getProperty("total_count");
			writer.append(String.valueOf(value16));
			writer.append("\n");

			// 017-Rotation failed files
			SQL_Test.RotationFailed();
			writer.append("017-Rotation failed files_count");
			writer.append(",");
			String value17 = System.getProperty("total_count");
			writer.append(String.valueOf(value17));
			writer.append("\n");

			// SQL_Test.Conversionfailedfiles();
			// writer.append("018-Conversion failed files");
			// writer.append(",");
			// String value18 = System.getProperty("total_count");
			// writer.append(String.valueOf(value18));
			// writer.append("\n");
			// SQL_Test.Rotationfailedfiles();

			// 020-Conversion Success but meta data extraction failed_Count
			SQL_Test.ConversionSuccessbutmetadataextractionfailed_Count();
			writer.append("020-Conversion Success but meta data extraction failed_Count");
			writer.append(",");
			String value20 = System.getProperty("total_count");
			writer.append(String.valueOf(value20));
			writer.append("\n");

			// SQL_Test.ConversionSuccessbutmetadataextractionfailed_files();
			
			
			// 005-Conversion service for the projects module_DocumentUploadSession
			SQL_Test.PR_TMPProjectDocumentUploadSession();
			writer.append("005-Conversion service for the projectsmodule_DocumentUploadSession");
			writer.append(",");
			String value5 = System.getProperty("total_count");
			writer.append(String.valueOf(value5));
			writer.append("\n");

			// 006-Conversion service for the projects module_Document to be processed
			SQL_Test.Documenttobeprocessed();
			writer.append("006-Conversion service for the projects module_Document to be processed");
			writer.append(",");
			String value6 = System.getProperty("total_count");
			writer.append(String.valueOf(value6));
			writer.append("\n");
			
			// 010-Conversion service for the projects module_Processed As SpecialFile
			SQL_Test.DocumentProcessedAsSpecialFile();
			writer.append("010-Conversion service for the projects module_Processed As SpecialFile");
			writer.append(",");
			String value10 = System.getProperty("total_count");
			writer.append(String.valueOf(value10));
			writer.append("\n");
			
			
			writer.append("TITLE BLOCK EXTRACTION SERVICES");
			writer.append("\n");
			
			
			// 023-Title block In progress
			SQL_Test.TitleblockInprogress();
			writer.append("023-Title block Inprogress");
			writer.append(",");
			String value23 = System.getProperty("total_count");
			writer.append(String.valueOf(value23));
			writer.append("\n");
			
			
			// 025-Title block Failed files count
			SQL_Test.TitleblockFailed();
			writer.append("025-Title block Failed files count");
			writer.append(",");
			String value25 = System.getProperty("total_count");
			writer.append(String.valueOf(value25));
			writer.append("\n");

			// 022-Title block Pending
			SQL_Test.TitleblockPending();
			writer.append("022-Title block Pending ");
			writer.append(",");
			String value22 = System.getProperty("total_count");
			writer.append(String.valueOf(value22));
			writer.append("\n");
			

			// 024-Title block Passed
			SQL_Test.TitleblockPassed();
			writer.append("024-Title block Passed");
			writer.append(",");
			String value24 = System.getProperty("total_count");
			writer.append(String.valueOf(value24));
			writer.append("\n");
		
		
			
			writer.append("OCR EXTRACTION");
			writer.append("\n");
			
			
			// 028-OCR extraction InProcess for F&A
			SQL_Test.OCRextractionInProcess_FnA();
			writer.append("028-OCR extraction InProcess for F&A");
			writer.append(",");
			String value28 = System.getProperty("total_count");
			writer.append(String.valueOf(value28));
			writer.append("\n");
			
			
			// 031-OCR extraction Failed for F&A
			SQL_Test.OCRextractionFailed_FnA();
			writer.append("031-OCR extraction Failed for F&A");
			writer.append(",");
			String value31 = System.getProperty("total_count");
			writer.append(String.valueOf(value31));
			writer.append("\n");
			
			// 26-OCR extraction Pending for F&A
			SQL_Test.OCRextractionPending_FnA();
			writer.append("026-OCR extraction Pending for F&A");
			writer.append(",");
			String value26 = System.getProperty("total_count");
			writer.append(String.valueOf(value26));
			writer.append("\n");
			
									
			// 033-OCR extraction Passed for F&A
			SQL_Test.OCRextractionpassed_FnA();
			writer.append("033-OCR extraction Passed for F&A");
			writer.append(",");
			String value33 = System.getProperty("total_count");
			writer.append(String.valueOf(value33));
			writer.append("\n");
						
			
			
			// 029-OCR extraction InProcess for PROJECTS
			SQL_Test.OCRextractionInProcess_PROJECTS();
			writer.append("029-OCR extraction InProcess for PROJECTS");
			writer.append(",");
			String value29 = System.getProperty("total_count");
			writer.append(String.valueOf(value29));
			writer.append("\n");
						
						
			// 030-OCR extraction Failed for Projects
			SQL_Test.OCRextractionFailed_Projects();
			writer.append("030-OCR extraction Failed for Projects");
			writer.append(",");
			String value30 = System.getProperty("total_count");
			writer.append(String.valueOf(value30));
			writer.append("\n");	
						
			
			// 027-OCR extraction Pending for Projects
			SQL_Test.OCRextractionPending_PROJECTS();
			writer.append("027-OCR extraction Pending for Projects");
			writer.append(",");
			String value27 = System.getProperty("total_count");
			writer.append(String.valueOf(value27));
			writer.append("\n");
			
				
			// 032-OCR extraction passed for PROJECTS
			SQL_Test.OCRextractionpassed_PROJECTS();
			writer.append("032-OCR extraction passed for PROJECTS");
			writer.append(",");
			String value32 = System.getProperty("total_count");
			writer.append(String.valueOf(value32));
			writer.append("\n");
			
			
			
			writer.append("FLATTEN SERVICES_F&A");
			writer.append("\n");
			
			
			// 035-Flatten zip Inprogress for F&A
			SQL_Test.FlattenzipInprogress_FnA();
			writer.append("035-Flatten zip Inprogress for F&A");
			writer.append(",");
			String value35 = System.getProperty("total_count");
			writer.append(String.valueOf(value35));
			writer.append("\n");
			
			// 037-Flatten zip Failed for F&A
			SQL_Test.FlattenzipFailed_FnA();
			writer.append("037-Flatten zip Failed for F&A");
			writer.append(",");
			String value37 = System.getProperty("total_count");
			writer.append(String.valueOf(value37));
			writer.append("\n");
			
			// 034-Flatten zip Pending for F&A
			SQL_Test.FlattenzipPending_FnA();
			writer.append("034-Flatten zip Pending for F&A");
			writer.append(",");
			String value34 = System.getProperty("total_count");
			writer.append(String.valueOf(value34));
			writer.append("\n");
					
			
			// 036-Flatten zip Success for F&A
			SQL_Test.FlattenzipSuccess_FnA();
			writer.append("036-Flatten zip Success for F&A");
			writer.append(",");
			String value36 = System.getProperty("total_count");
			writer.append(String.valueOf(value36));
			writer.append("\n");
			
			
		
			
			writer.append("FLATTEN SERVICES_PROJECTS");
			writer.append("\n");
			
			// 039-Flatten zip Inprogress for PROJECTS
			SQL_Test.FlattenzipInprogress_PROJECTS();
			writer.append("039-Flatten zip Inprogress for PROJECTS");
			writer.append(",");
			String value39 = System.getProperty("total_count");
			writer.append(String.valueOf(value39));
			writer.append("\n");
			
			// 041-Flatten zip Failed for PROJECTS
			SQL_Test.FlattenzipFailed_PROJECTS();
			writer.append("041-Flatten zip Failed for PROJECTS");
			writer.append(",");
			String value41 = System.getProperty("total_count");
			writer.append(String.valueOf(value41));
			writer.append("\n");
			
			// 038-Flatten zip Pending for PROJECTS
			SQL_Test.FlattenzipPending_PROJECTS();
			writer.append("038-Flatten zip Pending for PROJECTS");
			writer.append(",");
			String value38 = System.getProperty("total_count");
			writer.append(String.valueOf(value38));
			writer.append("\n");
			
			
			
			// 040-Flatten zip Success for PROJECTS
			SQL_Test.FlattenzipSuccess_PROJECTS();
			writer.append("040-Flatten zip Success for PROJECTS");
			writer.append(",");
			String value40 = System.getProperty("total_count");
			writer.append(String.valueOf(value40));
			writer.append("\n");
			
			// 055-Conversion stuck for PROJECTS_PublishingOption_99
			SQL_Test.ConversionStuck_PROJECTS99();
			writer.append("055-Conversion stuck for PROJECTS_99");
			writer.append(",");
			String value55 = System.getProperty("total_count");
			writer.append(String.valueOf(value55));
			writer.append("\n");
			
			
			
			// 05-Conversion stuck for PROJECTS_PublishingOption_0
			SQL_Test.ConversionStuck_PROJECTS0();
			writer.append("056-Conversion stuck for PROJECTS_0");
			writer.append(",");
			String value56 = System.getProperty("total_count");
			writer.append(String.valueOf(value56));
			writer.append("\n");
			
			
	
			
			writer.append("\n");
			writer.append("\n");
			writer.append("UMQ RELATED QUEUES");
			writer.append("\n");
			
			
			
			RabbitMQ.browserLaunch();
			RabbitMQ.lauchApp();
			RabbitMQ.login();
			
		
			RabbitMQ.searchService1ReadyCount(value1);
			writer.append("042-Default-AimViewerPRD");
			writer.append(",");
			String value42 = System.getProperty("rabbit1");
			writer.append(String.valueOf(value42));
			writer.append("\n");
			
			
			
			RabbitMQ.searchService2ReadyCount(value1);
			writer.append("043-Default-AIMViewerRotationPRD");
			writer.append(",");
			String value43 = System.getProperty("rabbit1");
			writer.append(String.valueOf(value43));
			writer.append("\n");
			

			
			RabbitMQ.searchService3ReadyCount(value1);
			writer.append("044-Default-AIMStitchProductionNew");
			writer.append(",");
			String value44 = System.getProperty("rabbit1");
			writer.append(String.valueOf(value44));
			writer.append("\n");
			
			
			
			RabbitMQ.searchService4ReadyCount(value1);
			writer.append("045-Default-AutoHyperLinkPRD");
			writer.append(",");
			String value45 = System.getProperty("rabbit1");
			writer.append(String.valueOf(value45));
			writer.append("\n");
			
			
			
			RabbitMQ.searchService5ReadyCount(value1);
			writer.append("046-Default-AutoSavePRD");
			writer.append(",");
			String value46 = System.getProperty("rabbit1");
			writer.append(String.valueOf(value46));
			writer.append("\n");
			
			
			
			
			RabbitMQ.searchService6ReadyCount(value1);
			writer.append("047-Default-FlatFilePRDSc");
			writer.append(",");
			String value47 = System.getProperty("rabbit1");
			writer.append(String.valueOf(value47));
			writer.append("\n");
			
			
			
			RabbitMQ.searchService7ReadyCount(value1);
			writer.append("048-Default-FlatFileProjectsPRD");
			writer.append(",");
			String value48 = System.getProperty("rabbit1");
			writer.append(String.valueOf(value48));
			writer.append("\n");
			
			
			RabbitMQ.searchService8ReadyCount(value1);
			writer.append("049-Default-InfolinkUploadTiles");
			writer.append(",");
			String value49 = System.getProperty("rabbit1");
			writer.append(String.valueOf(value49));
			writer.append("\n");
			
			
			RabbitMQ.searchService9ReadyCount(value1);
			writer.append("050-Default-MergeInfolink");
			writer.append(",");
			String value50 = System.getProperty("rabbit1");
			writer.append(String.valueOf(value50));
			writer.append("\n");
			
			
			
			RabbitMQ.searchService10ReadyCount(value1);
			writer.append("051-Default-PwcFileConversion");
			writer.append(",");
			String value51 = System.getProperty("rabbit1");
			writer.append(String.valueOf(value51));
			writer.append("\n");
			
			
			RabbitMQ.searchService11ReadyCount(value1);
			writer.append("052-Default-PwcTiffConversion");
			writer.append(",");
			String value52 = System.getProperty("rabbit1");
			writer.append(String.valueOf(value52));
			writer.append("\n");
			
			
			RabbitMQ.searchService12ReadyCount(value1);
			writer.append("053-Default-RegistrationInfo");
			writer.append(",");
			String value53 = System.getProperty("rabbit1");
			writer.append(String.valueOf(value53));
			writer.append("\n");
			
			RabbitMQ.searchService13ReadyCount(value1);
			writer.append("054-Default-SkysiteStitchProd");
			writer.append(",");
			String value54 = System.getProperty("rabbit1");
			writer.append(String.valueOf(value54));
			writer.append("\n");		
			
			
			RabbitMQ.closeApp();

			
			
			// Thread.sleep(1800000);
			// Thread.sleep(1200000);

			writer.close();
			Thread.sleep(3600000);
			if (i == 9999999) {
				System.out.println("Application Terminates");
				System.exit(0);
			}
		}

	}
}