package ServerMonitoring;

import org.openqa.selenium.WebDriver;

public class SQL_Test {

	// static File file = new File(Driver_ServerMonitoring.fileName);

	static WebDriver driver;


	public static void PendingImagetiles() throws Exception {
		
	
		System.out.println("Image tiles extraction execution is getting initiated");
		System.out.println("001-Image tiles extraction_Pending");
		String query1 = PropertyReader.getProperty("Newquery1");
		Replacedata.replace(query1);
		String mail1 = PropertyReader.getProperty("Mailbody1");
		String nquery1=PropertyReadertempfile.getProperty("query");
		ServerMonitoring.main(nquery1, mail1);
		System.out.println(System.getProperty("total_count"));
		Writedata.cleardata();

	}

	public static void InprogressImagetiles() throws Exception {

	
			System.out.println("002-Image tiles extraction_In progress");
			String query1 = PropertyReader.getProperty("Newquery2");
			Replacedata.replace(query1);
			String mail2 = PropertyReader.getProperty("Mailbody2");
			String nquery1=PropertyReadertempfile.getProperty("query");
			ServerMonitoring.main(nquery1, mail2);
			System.out.println(System.getProperty("total_count"));
			Writedata.cleardata();
		

	}

	public static void FailedImagetiles() throws Exception {

			System.out.println("003-Image tiles extraction_Failed");
			
			String query1 = PropertyReader.getProperty("Newquery3");
			Replacedata.replace(query1);		
			String mail3 = PropertyReader.getProperty("Mailbody3");
			String nquery1=PropertyReadertempfile.getProperty("query");
			ServerMonitoring.failedqueries(nquery1, mail3);
			System.out.println(System.getProperty("total_count"));
			Writedata.cleardata();

	}

	
	public static void Successimagetilecount() throws Exception {

	
			System.out.println("004-Image tiles extraction_Success");
			String query1 = PropertyReader.getProperty("Newquery4");
			Replacedata.replace(query1);
			String mail4 = PropertyReader.getProperty("Mailbody4");
			String nquery1=PropertyReadertempfile.getProperty("query");
			ServerMonitoring.withoutmail(nquery1);
			Writedata.cleardata();


	}


		public static void PR_TMPProjectDocumentUploadSession() throws Exception {

			System.out.println("Conversion service for the projects module is getting initiated");
			System.out.println("005-Conversion service for the projects module_DocumentUploadSession");
			String query1 = PropertyReader.getProperty("Newquery5");
			Replacedata.replace(query1);
			String mail5 = PropertyReader.getProperty("Mailbody5");
			String nquery1=PropertyReadertempfile.getProperty("query");
			ServerMonitoring.failedqueries(nquery1, mail5);
			Writedata.cleardata();
	
	}

	
	public static void Documenttobeprocessed() throws Exception {
	
			System.out.println("006-Conversion service for the projects module_Document to be processed");
			String query1 = PropertyReader.getProperty("Newquery6");
			Replacedata.replace(query1);
			String mail6 = PropertyReader.getProperty("Mailbody6");
			String nquery1=PropertyReadertempfile.getProperty("query");
			ServerMonitoring.main(nquery1, mail6);
			Writedata.cleardata();

	}


	public static void Documentinprocess() throws Exception {
		
			System.out.println("007-Conversion service for the projects module_Documentinprocess");
			String query1 = PropertyReader.getProperty("Newquery7");
			Replacedata.replace(query1);
			String mail7 = PropertyReader.getProperty("Mailbody7");
			String nquery1=PropertyReadertempfile.getProperty("query");
			ServerMonitoring.main(nquery1, mail7);
			Writedata.cleardata();
	}

	
	public static void DocumentOCRPending() throws Exception {
	
			System.out.println("008-Conversion service for the projects module_OCR Pending");
			String query1 = PropertyReader.getProperty("Newquery8");
			Replacedata.replace(query1);
			String mail8 = PropertyReader.getProperty("Mailbody8");
			String nquery1=PropertyReadertempfile.getProperty("query");
			ServerMonitoring.main(nquery1, mail8);
			Writedata.cleardata();
			
	
	}

	
	
	public static void DocumentCompleted() throws Exception {
	
			System.out.println("009-Conversion service for the projects module_Completed");
			String query1 = PropertyReader.getProperty("Newquery9");
			Replacedata.replace(query1);
			String nquery1=PropertyReadertempfile.getProperty("query");
			ServerMonitoring.withoutmail(nquery1);
			Writedata.cleardata();

	}

	
	public static void DocumentProcessedAsSpecialFile() throws Exception {
		
			System.out.println("010-Conversion service for the projects module_Processed As SpecialFile");
			String query1 = PropertyReader.getProperty("Newquery10");
			Replacedata.replacespecialfile(query1);
			String mail10 = PropertyReader.getProperty("Mailbody10");
			String nquery1=PropertyReadertempfile.getProperty("query");
			ServerMonitoring.main(nquery1, mail10);
			Writedata.cleardata();

		
	}


	public static void DocumentFailed() throws Exception {
		
			System.out.println("011-Conversion service for the projects module_Failed");
			String query1 = PropertyReader.getProperty("Newquery11");
			Replacedata.replace(query1);
			String mail11 = PropertyReader.getProperty("Mailbody11");
			String nquery1=PropertyReadertempfile.getProperty("query");
			ServerMonitoring.failedqueries(nquery1, mail11);
			Writedata.cleardata();


	}


	public static void DocumentFileSizeZero() throws Exception {
		
			System.out.println("012-Conversion service for the projects module_FileSizeZero");
			String query1 = PropertyReader.getProperty("Newquery12");
			Replacedata.replace(query1);
			String mail12 = PropertyReader.getProperty("Mailbody12");
			String nquery1=PropertyReadertempfile.getProperty("query");
			ServerMonitoring.main(nquery1, mail12);
			Writedata.cleardata();


	}

	
	public static void DocumentUploadFailed() throws Exception {
		
			System.out.println("013-Conversion service for the projects module_UploadFailed");
			String query1 = PropertyReader.getProperty("Newquery13");
			Replacedata.replace(query1);
			String mail13 = PropertyReader.getProperty("Mailbody13");
			String nquery1=PropertyReadertempfile.getProperty("query");
			ServerMonitoring.failedqueries(nquery1, mail13);
			Writedata.cleardata();


	}


	public static void RotationToBeProcessed() throws Exception {
	
			System.out.println("Rotation service is getting initiated");
			System.out.println("014-Rotation files to be Processed");
			String query1 = PropertyReader.getProperty("Newquery14");
			Replacedata.replace(query1);
			String mail14 = PropertyReader.getProperty("Mailbody14");
			String nquery1=PropertyReadertempfile.getProperty("query");
			ServerMonitoring.main(nquery1, mail14);
			Writedata.cleardata();

	}

	
	public static void RotationInProcess() throws Exception {
		
			System.out.println("015-Rotation files in Process");
			String query1 = PropertyReader.getProperty("Newquery15");
			Replacedata.replace(query1);
			String mail15 = PropertyReader.getProperty("Mailbody15");
			String nquery1=PropertyReadertempfile.getProperty("query");
			ServerMonitoring.main(nquery1, mail15);
			Writedata.cleardata();


	}


	public static void RotationCompleted() throws Exception {

			System.out.println("016-Rotation files Completed");
			String query1 = PropertyReader.getProperty("Newquery16");
			Replacedata.replace(query1);
			String nquery1=PropertyReadertempfile.getProperty("query");
			ServerMonitoring.withoutmail(nquery1);
			Writedata.cleardata();


	}

	public static void RotationFailed() throws Exception {
	
			System.out.println("017-Rotation failed files_count");
			String query1 = PropertyReader.getProperty("Newquery17");
			Replacedata.replace(query1);
			String mail17 = PropertyReader.getProperty("Mailbody17");
			String nquery1=PropertyReadertempfile.getProperty("query");
			ServerMonitoring.failedqueries(nquery1, mail17);
			Writedata.cleardata();


	}


	public static void Conversionfailedfiles() throws Exception {


			System.out.println("018-Conversion failed files");
			String query1 = PropertyReader.getProperty("Newquery18");
			Replacedata.replace(query1);
			String mail18 = PropertyReader.getProperty("Mailbody18");
			String nquery1=PropertyReadertempfile.getProperty("query");
			ServerMonitoring.fullvalue(nquery1,mail18 );
			Writedata.cleardata();


	}


	public static void Rotationfailedfiles() throws Exception {
		
			System.out.println("019-Rotation failed files");
			String query1 = PropertyReader.getProperty("Newquery19");
			Replacedata.replace(query1);
			String mail19 = PropertyReader.getProperty("Mailbody19");
			String nquery1=PropertyReadertempfile.getProperty("query");
			ServerMonitoring.fullvalue(nquery1, mail19);
			Writedata.cleardata();


	}


	public static void ConversionSuccessbutmetadataextractionfailed_Count() throws Exception {
	
			System.out.println("020-Conversion Success but meta data extraction failed_Count");
			String query1 = PropertyReader.getProperty("Newquery20");
			Replacedata.replace(query1);
			String mail20 = PropertyReader.getProperty("Mailbody20");
			String nquery1=PropertyReadertempfile.getProperty("query");
			ServerMonitoring.main(nquery1, mail20);
			Writedata.cleardata();

	}

	
	public static void ConversionSuccessbutmetadataextractionfailed_files() throws Exception {
		
			System.out.println("021-Conversion Success but meta data extraction failed_files");
			String query1 = PropertyReader.getProperty("Newquery21");
			Replacedata.replace(query1);
			String nquery1=PropertyReadertempfile.getProperty("query");
			ServerMonitoring.withoutmail(nquery1);
			Writedata.cleardata();



	}

	public static void TitleblockPending() throws Exception {
	
			System.out.println("022-TitleblockPending from PR_TMPProjectDocument where TBOCRStatus = 0");
			String query1 = PropertyReader.getProperty("Newquery22");
			Replacedata.replace(query1);
			String mail22 = PropertyReader.getProperty("Mailbody22");
			String nquery1=PropertyReadertempfile.getProperty("query");
			ServerMonitoring.main(nquery1, mail22);
			Writedata.cleardata();

	}

	public static void TitleblockInprogress() throws Exception {
		
			System.out.println("023-TitleblockInprogress from PR_TMPProjectDocument where TBOCRStatus = 1");
			String query1 = PropertyReader.getProperty("Newquery23");
			Replacedata.replace(query1);
			String mail23 = PropertyReader.getProperty("Mailbody23");
			String nquery1=PropertyReadertempfile.getProperty("query");
			ServerMonitoring.main(nquery1, mail23);
			Writedata.cleardata();

	

	}

	public static void TitleblockPassed() throws Exception {
	
			System.out.println("024-TitleblockPassed from PR_TMPProjectDocument where TBOCRStatus = 2");
			String query1 = PropertyReader.getProperty("Newquery24");
			Replacedata.replace(query1);
			String nquery1=PropertyReadertempfile.getProperty("query");
			ServerMonitoring.withoutmail(nquery1);
			Writedata.cleardata();


	}

	public static void TitleblockFailed() throws Exception {
		
			System.out.println("025-Title block Failed files");
			String query1 = PropertyReader.getProperty("Newquery25");
			Replacedata.replace(query1);
			String mail25 = PropertyReader.getProperty("Mailbody25");
			String nquery1=PropertyReadertempfile.getProperty("query");
			ServerMonitoring.failedqueries(nquery1, mail25);
			Writedata.cleardata();

	
	}
	

	public static void OCRextractionPending_FnA () throws Exception {
		
		System.out.println("026-OCR extraction Pending for F&A");
		String query1 = PropertyReader.getProperty("Newquery26");
		Replacedata.replace(query1);
		String mail26 = PropertyReader.getProperty("Mailbody26");
		String nquery1=PropertyReadertempfile.getProperty("query");
		ServerMonitoring.main(nquery1, mail26);
		Writedata.cleardata();


}
	
	
	public static void OCRextractionPending_PROJECTS () throws Exception {
		
		System.out.println("027-OCR extraction Pending for Projects");
		String query1 = PropertyReader.getProperty("Newquery27");
		Replacedata.replace(query1);
		String mail27 = PropertyReader.getProperty("Mailbody27");
		String nquery1=PropertyReadertempfile.getProperty("query");
		ServerMonitoring.main(nquery1, mail27);
		Writedata.cleardata();


}
	
	
	public static void OCRextractionInProcess_FnA () throws Exception {
		
		System.out.println("028-OCR extraction InProcess for F&A");
		String query1 = PropertyReader.getProperty("Newquery28");
		Replacedata.replace(query1);
		String mail28 = PropertyReader.getProperty("Mailbody28");
		String nquery1=PropertyReadertempfile.getProperty("query");
		ServerMonitoring.main(nquery1, mail28);
		Writedata.cleardata();


}
	
	
	public static void OCRextractionInProcess_PROJECTS () throws Exception {
		
		System.out.println("029-OCR extraction InProcess for PROJECTS");
		String query1 = PropertyReader.getProperty("Newquery29");
		Replacedata.replace(query1);
		String mail29 = PropertyReader.getProperty("Mailbody29");
		String nquery1=PropertyReadertempfile.getProperty("query");
		ServerMonitoring.main(nquery1, mail29);
		Writedata.cleardata();


}
	
	public static void OCRextractionFailed_Projects() throws Exception {
		
		System.out.println("030-OCR extraction Failed for Projects");
		String query1 = PropertyReader.getProperty("Newquery30");
		Replacedata.replace(query1);
		String mail30 = PropertyReader.getProperty("Mailbody30");
		String nquery1=PropertyReadertempfile.getProperty("query");
		ServerMonitoring.failedqueries(nquery1, mail30);
		Writedata.cleardata();


}
	
	
	public static void OCRextractionFailed_FnA() throws Exception {
		
		System.out.println("031-OCR extraction Failed for F&A");
		String query1 = PropertyReader.getProperty("Newquery31");
		Replacedata.replace(query1);
		String mail31 = PropertyReader.getProperty("Mailbody31");
		String nquery1=PropertyReadertempfile.getProperty("query");
		ServerMonitoring.failedqueries(nquery1, mail31);
		Writedata.cleardata();


}
	
	
	public static void OCRextractionpassed_PROJECTS() throws Exception {
		
		System.out.println("032-OCR extraction passed for PROJECTS");
		String query1 = PropertyReader.getProperty("Newquery32");
		Replacedata.replace(query1);
		String nquery1=PropertyReadertempfile.getProperty("query");
		ServerMonitoring.withoutmail(nquery1);
		Writedata.cleardata();

}
	
	
	public static void OCRextractionpassed_FnA() throws Exception {
		
		System.out.println("033-OCR extraction Passed for F&A");
		String query1 = PropertyReader.getProperty("Newquery33");
		Replacedata.replace(query1);
		String nquery1=PropertyReadertempfile.getProperty("query");
		ServerMonitoring.withoutmail(nquery1);
		Writedata.cleardata();

}
	
	public static void FlattenzipPending_FnA () throws Exception {
		
		System.out.println("034-Flatten zip Pending for F&A");
		String query1 = PropertyReader.getProperty("Newquery34");
		Replacedata.replace(query1);
		String mail34 = PropertyReader.getProperty("Mailbody34");
		String nquery1=PropertyReadertempfile.getProperty("query");
		ServerMonitoring.main(nquery1, mail34);
		Writedata.cleardata();

}

	
	public static void FlattenzipInprogress_FnA  () throws Exception {
		
		System.out.println("035-Flatten zip Inprogress for F&A");
		String query1 = PropertyReader.getProperty("Newquery35");
		Replacedata.replace(query1);
		String mail35 = PropertyReader.getProperty("Mailbody35");
		String nquery1=PropertyReadertempfile.getProperty("query");
		ServerMonitoring.main(nquery1, mail35);
		Writedata.cleardata();

}
	
	public static void FlattenzipSuccess_FnA  () throws Exception {
		
		System.out.println("036-Flatten zip Success for F&A");
		String query1 = PropertyReader.getProperty("Newquery36");
		Replacedata.replace(query1);
		String mail36 = PropertyReader.getProperty("Mailbody36");
		String nquery1=PropertyReadertempfile.getProperty("query");
		ServerMonitoring.withoutmail(nquery1);
		Writedata.cleardata();

}
	
	public static void FlattenzipFailed_FnA() throws Exception {
		
		System.out.println("037-Flatten zip Failed for F&A");
		String query1 = PropertyReader.getProperty("Newquery37");
		Replacedata.replace(query1);
		String mail37 = PropertyReader.getProperty("Mailbody37");
		String nquery1=PropertyReadertempfile.getProperty("query");
		ServerMonitoring.failedqueries(nquery1, mail37);
		Writedata.cleardata();


}
	
	public static void FlattenzipPending_PROJECTS () throws Exception {
		
		System.out.println("038-Flatten zip Pending for PROJECTS");
		String query1 = PropertyReader.getProperty("Newquery38");
		Replacedata.replace(query1);
		String mail38 = PropertyReader.getProperty("Mailbody38");
		String nquery1=PropertyReadertempfile.getProperty("query");
		ServerMonitoring.main(nquery1, mail38);
		Writedata.cleardata();

}

	
	public static void FlattenzipInprogress_PROJECTS  () throws Exception {
		
		System.out.println("039-Flatten zip Inprogress for PROJECTS");
		String query1 = PropertyReader.getProperty("Newquery39");
		Replacedata.replace(query1);
		String mail39 = PropertyReader.getProperty("Mailbody39");
		String nquery1=PropertyReadertempfile.getProperty("query");
		ServerMonitoring.main(nquery1, mail39);
		Writedata.cleardata();

}
	
	public static void FlattenzipSuccess_PROJECTS  () throws Exception {
		
		System.out.println("040-Flatten zip Success for PROJECTS");
		String query1 = PropertyReader.getProperty("Newquery40");
		Replacedata.replace(query1);
		String mail40 = PropertyReader.getProperty("Mailbody40");
		String nquery1=PropertyReadertempfile.getProperty("query");
		ServerMonitoring.withoutmail(nquery1);
		Writedata.cleardata();

}
	
	public static void FlattenzipFailed_PROJECTS() throws Exception {
		
		System.out.println("041-Flatten zip Failed for PROJECTS");
		String query1 = PropertyReader.getProperty("Newquery41");
		Replacedata.replace(query1);
		String mail41 = PropertyReader.getProperty("Mailbody41");
		String nquery1=PropertyReadertempfile.getProperty("query");
		ServerMonitoring.failedqueries(nquery1, mail41);
		Writedata.cleardata();
		
}
	
	//As per Vamshi's suggestion this is added on 22-Nov-2018
	
	public static void ConversionStuck_PROJECTS99() throws Exception {
		
		System.out.println("055-Conversion stuck for PROJECTS_PublishingOption_99");
		String query1 = PropertyReader.getProperty("Newquery55");
		Replacedata.replace(query1);
		String mail55 = PropertyReader.getProperty("Mailbody55");
		String nquery1=PropertyReadertempfile.getProperty("query");
		ServerMonitoring.failedqueries(nquery1, mail55);
		Writedata.cleardata();
		
}
	
	//As per Vamshi's suggestion this is added on 26-Nov-2018
	
	public static void ConversionStuck_PROJECTS0() throws Exception {
		
		System.out.println("056-Conversion stuck for PROJECTS_PublishingOption_0");
		String query1 = PropertyReader.getProperty("Newquery56");
		Replacedata.replace(query1);
		String mail56 = PropertyReader.getProperty("Mailbody56");
		String nquery1=PropertyReadertempfile.getProperty("query");
		ServerMonitoring.failedqueries(nquery1, mail56);
		Writedata.cleardata();
		
}
}