package ServerMonitoring;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class test {
	
	static WebDriver driver;
	
	public static void main(String[] args) throws IOException {
		File dest = new File("C:\\Users\\trinanjwan.sarkar\\eclipse-workspace\\Trinanjwan's_ServerMonitoringProject\\Driver\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver();
		driver.get(PropertyReader.getProperty("RabbitMQURL"));
	}

}
