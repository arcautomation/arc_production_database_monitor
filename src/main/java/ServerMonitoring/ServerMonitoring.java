package ServerMonitoring;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ServerMonitoring {

	// private static int row;

	public static void main(String query1, String servicename) throws ClassNotFoundException, SQLException {

		// Connection URL Syntax: "jdbc:mysql://ipaddress:portnumber/db_name"

		String dbUrl = "jdbc:sqlserver://10.1.1.56;databaseName=CollaborationGlobal";

		// Staging
		// String dbUrl =
		// "jdbc:sqlserver://10.99.10.78;databaseName=CollaborationGlobal";

		// Database Username

		// This is for Staging
		// String username = "mpt_db_user";
		String username = "Pw_dev";

		// Database Password

		// This is for Staging
		// String password = "mptdbuser";

		String password = "PW!nd!adev";

		// Load mysql jdbc driver
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

		// Create Connection to DB
		Connection con = DriverManager.getConnection(dbUrl, username, password);
		System.out.println("The con is:"+con);

		// Create Statement Object
		Statement stmt = con.createStatement();
	

		// Execute the SQL Query. Store results in ResultSet
		ResultSet rs = stmt.executeQuery(query1);

		int total_count=0;
		String row1 = null;
		while (rs.next()) {
			row1 = rs.getString(1);

		

			// Log.message(row1);
			 
			 System.setProperty("total_count", row1);
			 System.out.println(System.getProperty("total_count"));
			 total_count = Integer.parseInt(row1);
			 System.out.println(System.getProperty("total_count"));
			if (total_count > 200) {

				String line1 = "Count goes more than 200 for the QUEUE" + " " + servicename + '\n' + "Count is:" +total_count 
						+'\n'+'\n'+"Regards,"+ '\n' + "ARC AUTOMATION TEAM";

				Mailtrigger.main(line1, servicename);

			}
			
			

		}
		con.close();
		
		
	}

	public static void withoutmail(String query1) throws ClassNotFoundException, SQLException {

		// Connection URL Syntax: "jdbc:mysql://ipaddress:portnumber/db_name"
		String dbUrl = "jdbc:sqlserver://10.1.1.56;databaseName=CollaborationGlobal";

		// Database Username
		String username = "Pw_dev";

		// Database Password
		String password = "PW!nd!adev";

		// Load mysql jdbc driver
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

		// Create Connection to DB
		Connection con = DriverManager.getConnection(dbUrl, username, password);

		// Create Statement Object
		Statement stmt = con.createStatement();

		// Execute the SQL Query. Store results in ResultSet
		ResultSet rs = stmt.executeQuery(query1);

		int total_count2=0;
		String row1 = null;
		while (rs.next()) {
			String row12 = rs.getString(1);

			System.out.println(row12);
			 System.setProperty("total_count", row12);
			 total_count2 = Integer.parseInt(row12);
			 System.out.println(System.getProperty("total_count"));
			
		}

		con.close();

	}

	public static void fullvalue(String query1, String mail25) throws ClassNotFoundException, SQLException {

		// Connection URL Syntax: "jdbc:mysql://ipaddress:portnumber/db_name"
		String dbUrl = "jdbc:sqlserver://10.1.1.56;databaseName=CollaborationGlobal";

		// Database Username
		String username = "Pw_dev";

		// Database Password
		String password = "PW!nd!adev";

		// Load mysql jdbc driver
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

		// Create Connection to DB
		Connection con = DriverManager.getConnection(dbUrl, username, password);

		// Create Statement Object
		Statement stmt = con.createStatement();

		// Execute the SQL Query. Store results in ResultSet
		ResultSet rs = stmt.executeQuery(query1);
		int total_count1=0;
		int total_count2=0;
		int total_count3=0;
		String row1 = null;

		while (rs.next()) {
			String row11 = rs.getString(1);
			String row2 = rs.getString(2);
			String row3 = rs.getString(3);
			
			System.out.println(row11);
			System.out.println(row2);
			System.out.println(row3);
			System.setProperty("total_count", row11+"|"+row2+"|"+row3);
			total_count1 = Integer.parseInt(row11);
			total_count2 = Integer.parseInt(row2);
			total_count3 = Integer.parseInt(row3);

			

			String line1 = "Find the list of Title block failed files";
			Mailtrigger.main(line1, line1);

		}

		// closing DB Connection
		con.close();

	}

	public static void failedqueries(String query1, String servicename) throws ClassNotFoundException, SQLException {

		// Connection URL Syntax: "jdbc:mysql://ipaddress:portnumber/db_name"

		String dbUrl = "jdbc:sqlserver://10.1.1.56;databaseName=CollaborationGlobal";

		// Staging
		// String dbUrl =
		// "jdbc:sqlserver://10.99.10.78;databaseName=CollaborationGlobal";

		// Database Username

		// This is for Staging
		// String username = "mpt_db_user";
		String username = "Pw_dev";

		// Database Password

		// This is for Staging
		// String password = "mptdbuser";

		String password = "PW!nd!adev";

		// Load mysql jdbc driver
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

		// Create Connection to DB
		Connection con = DriverManager.getConnection(dbUrl, username, password);
		

		// Create Statement Object
		Statement stmt = con.createStatement();
	

		// Execute the SQL Query. Store results in ResultSet
		ResultSet rs = stmt.executeQuery(query1);
		System.out.println("+++++++++++++"+rs);
		
		int total_count=0;
		String row1 = null;
		while (rs.next()) {
			System.out.println(rs.getString(1)+"+++++++++++++++++++++++++++++++++");
			row1 = rs.getString(1);
			System.setProperty("total_count", row1);
			 System.out.println(System.getProperty("total_count"));
			 total_count = Integer.parseInt(row1);
			 System.out.println(System.getProperty("total_count"));


			if (total_count > 0) {

				String line1 = "Count goes more than 0(zero) for the QUEUE" + " " + servicename + '\n' + "Count is:" +total_count 
						+'\n'+'\n'+"Regards,"+ '\n' + "ARC AUTOMATION TEAM";

				Mailtrigger.main(line1, servicename);

			}

			

		}
		// closing DB Connection
					con.close();
					
	}				
	
	
						//Get time
					public static String getdate() throws ClassNotFoundException, SQLException {

						// Connection URL Syntax: "jdbc:mysql://ipaddress:portnumber/db_name"

						String dbUrl = "jdbc:sqlserver://10.1.1.56;databaseName=CollaborationGlobal";

						// Staging
						// String dbUrl =
						// "jdbc:sqlserver://10.99.10.78;databaseName=CollaborationGlobal";

						// Database Username

						// This is for Staging
						// String username = "mpt_db_user";
						String username = "Pw_dev";

						// Database Password

						// This is for Staging
						// String password = "mptdbuser";

						String password = "PW!nd!adev";

						// Load mysql jdbc driver
						Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

						// Create Connection to DB
						Connection con = DriverManager.getConnection(dbUrl, username, password);
						

						// Create Statement Object
						Statement stmt = con.createStatement();
					

						// Execute the SQL Query. Store results in ResultSet
						ResultSet rs = stmt.executeQuery("SELECT CONVERT (date, SYSDATETIME())");
						
						String row1 = null;
						while (rs.next()) {
							row1 = rs.getString(1);
							System.out.println("System date is:"+row1);
	
							
					

							

						}
						// closing DB Connection
									con.close();
									return row1;				
							
					
	}
}
